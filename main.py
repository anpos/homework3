import pandas as pd
import cartopy.crs as ccrs
import cartopy.feature as cf
import matplotlib.pyplot as plt
import matplotlib


def plot_direct_flight_map(extent: tuple, data_frame: pd.DataFrame, out_filename: str, show_image: bool, **kwargs):
    """
    Plot flight map given map extent and flight table. Save image file.
    Data needs to define City, Latitude and Longitude.
    First row in data should be source location.
    In future, extent could be derived from flight table.

    :param extent: tuple of left, right, bottom, top value of the map
    :param data_frame: flight data returned by pandas.read_csv()
    :param out_filename: image filename for output map
    :param kwargs: column names in data_frame, args: col_lat, col_lng, col_cty
                   map colors, edge widths, see args below...
    :param show_image: True to show image
    :return: None
    """
    col_lat = kwargs.get('col_lat', 'Latitude')
    col_lng = kwargs.get('col_lng', 'Longitude')
    col_cty = kwargs.get('col_cty', 'City')

    marker_color = kwargs.get('marker_color', '#cc5500')
    water_color = kwargs.get('water_color', '#d8d8ff')
    text_color = kwargs.get('text_color', '#000000')
    land_color = kwargs.get('land_color', '#b0b0b0')
    edge_color = kwargs.get('edge_color', '#000000')  # for coastline edges
    edge_width = kwargs.get('edge_width', .1)
    line_color = kwargs.get('line_color', '#5555ff')  # for flight lines
    line_width = kwargs.get('line_width', .3)
    scale = kwargs.get('scale', '50m')

    ax = plt.axes(projection=ccrs.Robinson())

    ax.set_extent(extent)

    ocean = cf.NaturalEarthFeature('physical', 'ocean', scale, facecolor=water_color, linewidth=edge_width)
    ax.add_feature(ocean)

    land = cf.NaturalEarthFeature('physical', 'land', scale, facecolor=land_color, linewidth=edge_width)
    ax.add_feature(land)

    ax.coastlines(scale, color=edge_color, linewidth=edge_width)
    ax.add_feature(cf.LAKES, facecolor=water_color, edgecolor=edge_color, linewidth=edge_width)
    ax.add_feature(cf.BORDERS, edgecolor=edge_color, linewidth=edge_width)

    lat_src = data_frame.loc[data_frame.index[0], col_lat]
    lng_src = data_frame.loc[data_frame.index[0], col_lng]
    cty_src = data_frame.loc[data_frame.index[0], col_cty]

    for row in range(data_frame.shape[0]):
        lat_dst = data_frame.loc[data_frame.index[row], col_lat]
        lng_dst = data_frame.loc[data_frame.index[row], col_lng]
        cty_dst = data_frame.loc[data_frame.index[row], col_cty]

        plt.plot([lng_src, lng_dst], [lat_src, lat_dst],
                 color=line_color,
                 linewidth=line_width,
                 marker='o',
                 markerfacecolor=marker_color,
                 markeredgecolor=marker_color,
                 markersize=1,
                 transform=ccrs.Geodetic())

        plt.text(lng_dst, lat_dst, cty_dst,
                 color=text_color,
                 fontsize=4,
                 transform=ccrs.Geodetic())

    plt.title(f'Direct flights from {cty_src}')
    plt.savefig(out_filename, bbox_inches='tight', dpi=300)
    if show_image:
        plt.show()


if __name__ == '__main__':
    matplotlib.rcParams['figure.dpi'] = 300  # make plt.show() look nice

    a = pd.read_csv("otselennud.csv", sep=';')
    b = pd.read_csv("airports.dat", sep=',', usecols=['IATA', 'Latitude', 'Longitude'])
    merged_data = a.merge(b, on='IATA', how='left')

    plot_direct_flight_map((-20, 45, 30, 75), merged_data, 'tallinn_flight_map.png', True)
